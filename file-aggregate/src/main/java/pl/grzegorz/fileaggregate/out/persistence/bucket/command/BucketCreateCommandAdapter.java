package pl.grzegorz.fileaggregate.out.persistence.bucket.command;

import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.fileapplication.out.bucket.command.BucketCreateCommandPort;
import pl.grzegorz.filedomain.aggregates.BucketAggregate;
import pl.grzegorz.filedomain.exception.MinioException;

@Service
@RequiredArgsConstructor
@Slf4j
class BucketCreateCommandAdapter implements BucketCreateCommandPort {

    private final MinioClient minioClient;

    @Override
    public void create(BucketAggregate bucketAggregate) {
        MakeBucketArgs makeBucketArgs = MakeBucketArgs.builder()
                .bucket(bucketAggregate.name())
                .region(bucketAggregate.region())
                .build();
        try {
            minioClient.makeBucket(makeBucketArgs);
        } catch (Exception e) {
            log.error("Cannot create bucket; exceptionMessage -> {}", e.getMessage());
            throw new MinioException(e.getMessage());
        }
    }
}
