package pl.grzegorz.fileaggregate.out.persistence.bucket.query;

import io.minio.BucketExistsArgs;
import io.minio.MinioClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.fileapplication.out.bucket.query.BucketExistsQueryPort;
import pl.grzegorz.filedomain.exception.MinioException;

@Service
@RequiredArgsConstructor
@Slf4j
public class BucketExistsQueryAdapter implements BucketExistsQueryPort {

    private final MinioClient minioClient;

    @Override
    public Boolean existsByName(String bucketName) {
        BucketExistsArgs bucketExistsArgs = BucketExistsArgs.builder()
                .bucket(bucketName)
                .build();
        try {
            return minioClient.bucketExists(bucketExistsArgs);
        } catch (Exception e) {
            log.error("Cannot check exists of bucket; exception message: {}", e.getMessage());
            throw new MinioException(e.getMessage());
        }
    }
}