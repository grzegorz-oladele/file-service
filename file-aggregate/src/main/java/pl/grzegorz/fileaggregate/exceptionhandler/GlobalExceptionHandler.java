package pl.grzegorz.fileaggregate.exceptionhandler;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.grzegorz.filedomain.exception.BucketAlreadyExistsException;
import pl.grzegorz.filedomain.exception.MinioException;

@RestControllerAdvice
@RequiredArgsConstructor
class GlobalExceptionHandler {

    private final HttpServletRequest httpServletRequest;

    @ExceptionHandler(MinioException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse handleMinioException(MinioException e) {
        return ErrorResponse.toErrorMessage(e.getMessage(), getCurrentPath(), HttpStatus.BAD_REQUEST.value());
    }

    @ExceptionHandler(BucketAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    ErrorResponse handleBucketAlreadyExistsException(BucketAlreadyExistsException e) {
        return ErrorResponse.toErrorMessage(e.getMessage(), getCurrentPath(), HttpStatus.CONFLICT.value());
    }

    private String getCurrentPath() {
        return httpServletRequest.getContextPath();
    }
}