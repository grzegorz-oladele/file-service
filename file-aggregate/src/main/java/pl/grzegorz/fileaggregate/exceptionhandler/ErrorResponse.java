package pl.grzegorz.fileaggregate.exceptionhandler;

import lombok.AccessLevel;
import lombok.Builder;

import java.time.LocalDateTime;

@Builder(setterPrefix = "with", access = AccessLevel.PRIVATE)
record ErrorResponse(
        String message,
        String path,
        int responseCode,
        LocalDateTime timestamp
) {

    static ErrorResponse toErrorMessage(String message, String path, int responseCode) {
        return ErrorResponse.builder()
                .withMessage(message)
                .withPath(path)
                .withResponseCode(responseCode)
                .withTimestamp(LocalDateTime.now())
                .build();
    }
}