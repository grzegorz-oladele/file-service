package pl.grzegorz.fileaggregate.in.rest.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.fileaggregate.in.rest.dto.CreateBucketDto;

@RequestMapping("/files")
public interface CreateBucketApi {

    @PostMapping("/buckets")
    ResponseEntity<Void> createBucket(@RequestBody CreateBucketDto createBucketDto);
}