package pl.grzegorz.fileaggregate.in.rest.dto;

import pl.grzegorz.fileapplication.in.bucket.command.BucketCreateCommandUseCase.BucketCreateCommand;

public record CreateBucketDto(
        String name
) {

    public BucketCreateCommand toCreateCommand() {
        return BucketCreateCommand.builder()
                .withName(name)
                .build();
    }
}