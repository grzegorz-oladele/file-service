package pl.grzegorz.fileaggregate.in.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.fileaggregate.in.rest.api.CreateBucketApi;
import pl.grzegorz.fileaggregate.in.rest.dto.CreateBucketDto;
import pl.grzegorz.fileapplication.in.bucket.command.BucketCreateCommandUseCase;

@RestController
@RequiredArgsConstructor
class CreateBucketController implements CreateBucketApi {

    private final BucketCreateCommandUseCase bucketCreateCommandUseCase;

    @Override
    public ResponseEntity<Void> createBucket(CreateBucketDto createBucketDto) {
        bucketCreateCommandUseCase.create(createBucketDto.toCreateCommand());
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}