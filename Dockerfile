FROM amazoncorretto:17-alpine
EXPOSE 8600
WORKDIR /app
COPY file-service-server/target/file-service-server-0.0.1-SNAPSHOT.jar file-service-server-0.0.1-SNAPSHOT.jar
CMD ["java", "-jar", "file-service-server-0.0.1-SNAPSHOT.jar"]