package pl.grzegorz.fileapplication.service;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.fileapplication.in.bucket.command.BucketCreateCommandUseCase;
import pl.grzegorz.fileapplication.out.bucket.command.BucketCreateCommandPort;
import pl.grzegorz.fileapplication.out.bucket.query.BucketExistsQueryPort;
import pl.grzegorz.filedomain.aggregates.BucketAggregate;
import pl.grzegorz.filedomain.exception.BucketAlreadyExistsException;
import pl.grzegorz.filedomain.exception.ExceptionMessage;

@RequiredArgsConstructor
public class BucketCreateCommandService implements BucketCreateCommandUseCase {

    private final BucketExistsQueryPort bucketExistsQueryPort;
    private final BucketCreateCommandPort bucketCreateCommandPort;

    @Override
    public void create(BucketCreateCommand bucketCreateCommand) {
        var bucketAggregate = BucketAggregate.create(bucketCreateCommand.toCreateData());
        if (bucketExistsQueryPort.existsByName(bucketCreateCommand.name())) {
            throw new BucketAlreadyExistsException(String.format(
                    ExceptionMessage.BUCKET_ALREADY_EXISTS_EXCEPTION_MESSAGE.getMessage(), bucketAggregate.name()));
        }
        bucketCreateCommandPort.create(bucketAggregate);
    }
}