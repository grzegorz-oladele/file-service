package pl.grzegorz.fileapplication.in.bucket.command;

import lombok.Builder;
import pl.grzegorz.filedomain.data.BucketCreateData;

public interface BucketCreateCommandUseCase {

    void create(BucketCreateCommand bucketCreateCommand);

    @Builder(setterPrefix = "with")
    record BucketCreateCommand(
            String name
    ) {

        private static final String REGION = "Europe/Warsaw";

        public BucketCreateData toCreateData() {
            return BucketCreateData.builder()
                    .withName(name)
                    .withRegion(REGION)
                    .build();
        }
    }
}
