package pl.grzegorz.fileapplication.out.bucket.query;

public interface BucketExistsQueryPort {

    Boolean existsByName(String bucketName);
}