package pl.grzegorz.fileapplication.out.bucket.command;

import pl.grzegorz.filedomain.aggregates.BucketAggregate;

public interface BucketCreateCommandPort {

    void create(BucketAggregate bucketAggregate);
}