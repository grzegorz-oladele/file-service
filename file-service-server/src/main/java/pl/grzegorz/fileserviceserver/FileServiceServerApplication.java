package pl.grzegorz.fileserviceserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"pl.grzegorz", "pl.grzegorz.*"})
@ConfigurationPropertiesScan(basePackages = {"pl.grzegorz", "pl.grzegorz.*"})
public class FileServiceServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileServiceServerApplication.class, args);
    }
}