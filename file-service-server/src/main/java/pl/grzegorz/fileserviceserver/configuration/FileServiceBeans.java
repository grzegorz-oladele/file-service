package pl.grzegorz.fileserviceserver.configuration;

import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.grzegorz.fileapplication.in.bucket.command.BucketCreateCommandUseCase;
import pl.grzegorz.fileapplication.out.bucket.command.BucketCreateCommandPort;
import pl.grzegorz.fileapplication.out.bucket.query.BucketExistsQueryPort;
import pl.grzegorz.fileapplication.service.BucketCreateCommandService;

@Configuration
class FileServiceBeans {

    @Value("${minio.endpoint}")
    private String minioEndpoint;
    @Value("${minio.access-key}")
    private String minioAccessKey;
    @Value("${minio.secret-key}")
    private String minioSecretKey;


    @Bean
    public MinioClient minioClient() {
        return MinioClient.builder()
                .endpoint(minioEndpoint)
                .credentials(minioAccessKey, minioSecretKey)
                .build();
    }

    @Bean
    public BucketCreateCommandUseCase bucketCreateCommandUseCase(BucketExistsQueryPort bucketExistsQueryPort,
                                                                 BucketCreateCommandPort bucketCreateCommandPort) {
        return new BucketCreateCommandService(bucketExistsQueryPort, bucketCreateCommandPort);
    }
}