package pl.grzegorz.filedomain.data;

import lombok.Builder;

@Builder(setterPrefix = "with")
public record BucketCreateData(
        String name,
        String region
) {
}
