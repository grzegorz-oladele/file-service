package pl.grzegorz.filedomain.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ExceptionMessage {

    BUCKET_ALREADY_EXISTS_EXCEPTION_MESSAGE("Bucket using name -> [%s] already exists");

    private final String message;
}
