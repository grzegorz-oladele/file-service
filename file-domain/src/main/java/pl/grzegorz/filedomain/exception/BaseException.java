package pl.grzegorz.filedomain.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
abstract class BaseException extends RuntimeException {

    private final String message;
}