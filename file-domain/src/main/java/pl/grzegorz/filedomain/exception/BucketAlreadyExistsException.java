package pl.grzegorz.filedomain.exception;

public class BucketAlreadyExistsException extends BaseException{
    public BucketAlreadyExistsException(String message) {
        super(message);
    }
}