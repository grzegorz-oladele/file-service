package pl.grzegorz.filedomain.exception;

public class MinioException extends BaseException{
    public MinioException(String message) {
        super(message);
    }
}