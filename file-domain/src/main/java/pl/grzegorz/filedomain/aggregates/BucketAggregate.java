package pl.grzegorz.filedomain.aggregates;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import pl.grzegorz.filedomain.data.BucketCreateData;

@Getter
@Accessors(fluent = true)
@Builder(setterPrefix = "with", access = AccessLevel.PRIVATE)
public class BucketAggregate {


    private String name;
    private String region;

    public static BucketAggregate create(BucketCreateData bucketCreateData) {
        return BucketAggregate.builder()
                .withName(bucketCreateData.name())
                .withRegion(bucketCreateData.region())
                .build();
    }
}